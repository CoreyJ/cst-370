/*
* Title: BFS.cpp
* Abstract: implements the Breadth-First search (BFS) algorithm
* reads an input file name and a starting node from a user then displays
* a list of visited nodes.
* Author: Corey Johnson
* ID: 3333
* Date: 5/28/2019
*/

#include <iostream>
#include <fstream>
#include <string>
#include <queue>

using namespace std;


int main(){
    string fileName = "";
    
    cout << "Enter input file name: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
        int numbers;
        inputFile >> numbers;
        
        int* ary = new int[numbers * numbers];
        int* visited = new int[numbers];
        
        for(int i = 0; i < numbers; i++){
            visited[i] = 0;
        }
        
        for(int i = 0; i < numbers * numbers; i++){
            inputFile >> ary[i];
        }
        
        int startVertex;
        cout << "Enter a start vertex: ";
        cin >> startVertex;
        
        queue<int> bfsQ;
        
        bfsQ.push(startVertex);
        int current;
        
        while(!bfsQ.empty()){
            current = bfsQ.front();
            bfsQ.pop();
            cout << current << " ";
            visited[current] = 1;
            for(int i = 0; i < numbers; i++){
                if(ary[current * numbers + i] == 1 && visited[i] == 0){
                    bfsQ.push(i);
                }
            }
            
            if(!bfsQ.empty()){
                cout << "-> ";
            }
        }
        
        cout << endl;
        
        delete[] ary;
        delete[] visited;
    }
    else{// file fails to open
        cout << "Unable to open file" << endl; 
    }
    return 0;
}