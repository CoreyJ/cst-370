/*
* Title: assignment.cpp
* Abstract: A solution to the Assignment Problem
* Reads in assignment costs per person and outputs all permutations as well as
* the lowest total cost as a solution
* Author: Corey Johnson
* ID: 3333
* Date: 5/28/2019
*/

#include <iostream>
#include <queue>
#include <vector>

using namespace std;

void createPermutations(queue<int> &q, vector<int> s, vector<int> n, int l){
    if(l == 0){
        for(int i = 0; i < n.size(); i ++){
            q.push(n.at(i));
        }
    }
    else{
        for(int i = 0; i < l; i ++){
            vector<int> newS;
            for(int j = 0; j < l; j++){
                newS.push_back(s.at(j));
            }
            vector<int> newN;
            for(int j = 0; j < n.size(); j ++){
                newN.push_back(n.at(j));
            }
            newN.push_back(s.at(i));
            newS.erase(newS.begin() + i);
            createPermutations(q, newS, newN, l-1);
        }
    }
}


int main(){
    
    int jobs;
    
    cout << "Number of jobs: ";
    cin >> jobs;
    
    
    int* ary = new int[jobs * jobs];
    
    cout << "Enter assignment costs of " << jobs << " persons: " << endl;
    
    for(int i = 0; i < jobs; i++){
        cout << "Person " << i+1 << ": ";
        for(int j = 0; j < jobs; j++){
            cin >> ary[i * jobs + j];
        }
    }
    cout << endl;
    queue<int> assignmentQ;
    
    vector<int> set;
    for(int i = 1; i <= jobs; i ++){
        set.push_back(i);
    }
    vector<int> perm;
    
    createPermutations(assignmentQ, set, perm, jobs);
    
    int count = 1;
    int lowCost = 1000;
    int* lowArray = new int[jobs];
    int* currentArray = new int [jobs];
    
    while(!assignmentQ.empty()){
        int sum = 0;
        cout << "Permutation " << count << ": <";
        currentArray = new int [jobs];
        for(int i  = 0; i < jobs; i++){
            currentArray[i] = assignmentQ.front();
            sum += ary[(assignmentQ.front()-1) * jobs + i];
            cout << assignmentQ.front(); 
            assignmentQ.pop();
            if(i + 1 < jobs){
                cout << ", ";
            }
        }
        if(sum < lowCost){
            lowCost = sum;
            delete[] lowArray;
            lowArray = currentArray;
            currentArray = NULL;
        }
        else{
            delete[] currentArray;
        }
        
        cout << "> = > total cost: " << sum << endl;
    }
    
    cout << endl;
    cout << "Solution: <";
    for(int i = 0; i < jobs; i++){
        cout << lowArray[i];
        if(i+ 1 < jobs){
            cout << ", ";
        }
    }
    cout << "> => total cost: " << lowCost << endl;
    
    delete[] ary;
    
    return 0;
}