/*
* Title: ShortRep.cpp
* Abstract: Reads a filename from the user and displays the numbers read in 
* ascending order using a short representation of any sequential numbers
* read in to save space
* Author: Corey Johnson
* ID: 3333
* Date: 5/28/2019
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(){
    
    string fileName = "";
    
    cout << "Enter input file name: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
        
        int numbers;
        inputFile >> numbers;
        
        int* ary = new int[numbers];
        
        for(int x = 0; x < numbers; x++){
            inputFile >> ary[x];
        }
        
        inputFile.close();
        
        for(int i = 0; i < numbers; i++){
            for(int j = 0; j < numbers; j++){
                if(ary[i] < ary[j]){
                    int temp = ary[i];
                    ary[i] = ary[j];
                    ary[j] = temp;
                }
            }
        }
        
        int series = 0;
        for(int x = 0; x < numbers; x++){
            
            if(series == 0){
                cout << ary[x];
            }   
            if(x + 1 < numbers){
                if(ary[x+1] == ary[x] + 1){
                    series +=1;
                }
                else{
                    if(series > 0){
                        cout << "-" <<ary[x];
                    }
                    series = 0;
                    cout << " ";
                    
                }
            }
            else{
                if(series > 0){
                        cout << "-" <<ary[x];
                    }
                    series = 0;
                    cout << " ";
            }
            
        }
        cout << endl;
        
        
        delete[] ary;
    }
    else{// file fails to open
        cout << "Unable to open file"; 
    }
    
    
    return 0;
}