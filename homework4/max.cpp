/*
* Title: max.cpp
* Abstract: returns the index of the maximum value in an array
* Author: Corey Johnson
* ID: 3333
* Date: 5/28/2019
*/


#include <iostream>

using namespace std;

int maxPosition(int A[], int start, int end){
    if(start == end){
        return start;
    }
    else{
        int index1 = maxPosition(A, start, (start + end)/2);
        int index2 = maxPosition(A, (start + end)/2 + 1, end);
        return A[index1]>A[index2]?index1:index2;
    }
}

int main(){
    
     // Sample array with 8 elements (n = 8)..
    int A[8] = {5, 13, 23, 7, 6, 1, 8, 4};

    int maxIndex = maxPosition (A, 0, 7);

    cout << "Max = " << A[maxIndex] << endl;

    return 0;
}