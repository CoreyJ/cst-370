/*
* Title: heap.cpp
* Abstract: Reads in a set of numbers and ouputs if its a heap. If not a heap
* the program constructs a heap with the numbers.
* Author: Corey Johnson
* ID: 3333
* Date: 6/18/2019
*/

#include <iostream>
#include <cmath>

using namespace std;

void heapSwapUp(int* &heap, int i = 1){
    int j = floor(i/2);
    if(j == 0){
        return;
    }
    if(heap[i] > heap[j]){
        int temp = heap[j];
        heap[j] = heap[i];
        heap[i] = temp;
        heapSwapUp(heap, j);
    }
    return;
}
void heapSwapDown(int* heap, int size, int i = 1){
    if(i > floor(size/2)){
        return;
    }
    int j = 2 * i;
    if(j < size){
        if(heap[j] < heap[j+1]){
            j++;
        }
    }
    if(heap[i] < heap[j]){
        int temp = heap[j];
        heap[j] = heap[i];
        heap[i] = temp;
        heapSwapDown(heap, size, j);
    }
    return;
}

void heapInsert(int* &heap, int& size, int num){
    size++;
    int* newHeap = new int[size];
    int i;
    for(i = 1; i < size; i++){
        newHeap[i] = heap[i];
    }
    newHeap[size] = num;
    delete [] heap;
    heap = newHeap;
    heapSwapUp(heap, i);
}

void constructHeap(int* &heap, int size){
    bool heapBool;
    for(int i = floor(size/2); i >= 1; i--){
        int k = i;
        int v = heap[k];
        heapBool = false;
        while(!heapBool && 2 * k <= size){
            int j = 2 * k;
            if(j < size)//two children
                if(heap[j] < heap[j+1]){
                    j = j+1;
                }
            if(v >= heap[j]){
                heapBool = true;
            }
            else{
                heap[k] = heap[j];
                k = j;
            }
        }
        heap[k] = v;
    }
    cout << "Heap constructed: ";
    for(int i = 1; i <= size; i++){
        cout << heap[i] << " ";
    }
    cout << endl;
}

void checkHeap(int* &heap, int size){
    bool heapBool;
    for(int i = floor(size/2); i >= 1; i--){
        int k = i;
        int v = heap[k];
        heapBool = false;
        while(!heapBool && 2 * k <= size){
            int j = 2 * k;
            if(j < size){//two children
                if(heap[j] < heap[j+1]){ //check highest value child
                    j = j+1;
                }
            }
            if(v >= heap[j]){
                heapBool = true;
            }
            else{
                cout << "This is NOT a heap." << endl;
                constructHeap(heap, size);
                return;
            }
        }
    }
    cout << "This is a heap." << endl;
}

void deleteMax(int* &heap, int& size){
    int temp = heap[1];
    heap[1] = heap[size];
    heap[size] = temp;
    size--;
    int* newHeap = new int[size];
    for(int i = 1; i <= size; i++){
        newHeap[i] = heap[i];
    }
    
    delete[] heap;
    heap = newHeap;
    heapSwapDown(heap,size, 1);
}

void heapSort(int* &heap, int& size){
    cout << "Heapsort: ";
    while(size >=1){
        cout << heap[1] << " ";
        deleteMax(heap, size);
    }
    cout << endl;
}

int main(){
    
    int inputSize = 0;
    int menuOption = 0;
    bool heap = false;
    
    cout << "Input size: ";
    cin >> inputSize;
    
    
    int* inputArray = new int[inputSize + 1];
    cout << "Enter Numbers: ";
    for(int i = 1; i <= inputSize; i++){
        cin >> inputArray[i];
    }
    
    checkHeap(inputArray, inputSize);
    
    while (menuOption != 3){
        
        cout << "Select an operation" << endl;
        cout << "\t1: Insert a number" << endl;
        cout << "\t2: Delete the max" << endl;
        cout << "\t3: Heapsort & Quit" << endl;
        cin >> menuOption;
        
        switch(menuOption){
            case 1:
                cout << "Enter a number: ";
                int num;
                cin >> num;
                heapInsert(inputArray, inputSize, num);
                cout << "Updated heap: ";
                for(int i = 1; i <= inputSize; i++){
                    cout << inputArray[i] << " ";
                }
                cout << endl;
                break;
            case 2:
                deleteMax(inputArray, inputSize);
                cout << "Updated heap: ";
                for(int i = 1; i <= inputSize; i++){
                    cout << inputArray[i] << " ";
                }
                cout << endl;
                break;
            case 3:
                heapSort(inputArray, inputSize);
                // exits loop
                break;
            default:
                cout << "Invalid menu option" << endl;
        }
    }
    
    cout << "Bye!" << endl;
    
    delete[] inputArray;
    
    return 0;
}