/*
* Title: prim.cpp
* Abstract: This program uses Prim's algorithm to calculate the minimum
* spanning tree from an input graph.
* Author: Corey Johnson
* ID: 3333
* Date: 6/18/2019
*/

#include <iostream>
#include <fstream>

using namespace std;

int main(){
    
    string fileName = "";
    
    cout << "Enter filename: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
    
        int start= 0;
    
        int vertices;
        int edges;
        
        int visitedCount = 0;
        inputFile >> vertices;
        inputFile >> edges;
        
        int* startingVertex = new int[edges];
        int* endingVertex = new int[edges];
        int* cost = new int[edges];
        
        int* visited = new int[vertices];
    
        for(int i = 0; i < edges; i ++){
            inputFile >> startingVertex[i] >> endingVertex[i] >> cost[i];
        }
        int costMax = 0;
        for(int i = 0 ; i < edges; i ++){
            if(cost[i] > costMax){
                costMax = cost[i];
            }
        }
        
        cout << "Enter the first vertex to start: ";
        cin >> start;
        
        visited[0] = start;
        visitedCount++;
        
        cout << endl;
        
        while(visitedCount < vertices){
            bool found = false;
            int min = costMax + 1;
            int minVertex;
            
            for(int i = 0; i < visitedCount; i++){
                for(int j = 0; j < edges; j++){
                    if(startingVertex[j] == visited[i]){
                        bool end = true;
                        for(int x = 0; x < visitedCount; x++){
                            if(endingVertex[j] == visited[x]){
                                end = false;
                                break;
                            }
                        }
                        if(end){
                            if(found){
                                if(cost[j] < cost[min]){
                                    min = j;
                                    minVertex = endingVertex[j];
                                }
                            }
                            else{                        
                                min = j;
                                minVertex = endingVertex[j];
                                found = true;
                            }
                        }
                    }
                    if(endingVertex[j] == visited[i]){
                        bool end = true;
                        for(int x = 0; x < visitedCount; x++){
                            if(startingVertex[j] == visited[x]){
                                end = false;
                                break;
                            }
                        }
                        if(end){
                            if(found){
                                if(cost[j] < cost[min]){
                                    min = j;
                                    minVertex = startingVertex[j];
                                }
                            }
                            else{                        
                                min = j;
                                minVertex = startingVertex[j];
                                found = true;
                            }
                        }
                    }
                }
            }
            cout << "(" << visitedCount << ") New edge: " << startingVertex[min] 
            << ", " << endingVertex[min] << " - cost " << cost[min] << endl;
            visited[visitedCount] = minVertex;
            visitedCount++;
        }
        delete[] startingVertex; 
        delete[] endingVertex;
        delete[] cost;
        delete[] visited;
    }
    else{
        cout << "File not Found" << endl;
    }
    return 0;
}