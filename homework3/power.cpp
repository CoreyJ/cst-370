/*
* Title: power.cpp
* Abstract: computes 2^n for a nonnegative integer n with a recursive function
* Author: Corey Johnson
* ID: 3333
* Date: 5/14/2019
*/

#include <iostream>

using namespace std;

int powerOfTwo(int n){
    if (n == 0){
        return 1;
    }
    else{
        return powerOfTwo(n-1) + powerOfTwo(n-1);
    }
}

int main(){
    int n = 0;
    
    cout << "Enter a number: ";
    cin >> n;
    cout << "Result: " << powerOfTwo(n) << endl;
    
    return 0;
}