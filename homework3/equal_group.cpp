/*
* Title: equal_group.cpp
* Abstract: given n positive distinct integers, this program groups them into two 
* disjoint groups with the same sum if a solution exists.
* Author: Corey Johnson
* ID: 3333
* Date: 5/14/2019
*/

#include <iostream>
#include <set>
#include <cmath>

using namespace std;

int main(){
    
    int n = 0; // number of integers (user input)
    
    int mask = 1; // bit mask
    
    // group totals
    int groupZero = 0;
    int groupOne = 0;
    
    // sets to hold each group
    set<int> setZero;
    set<int> setOne;
    
    // flag for solution found
    bool found = false;
    
    // array to hold numbers input
    int* numbers;
    
    cout << "Number of input: ";
    cin >> n;
    
    numbers = new int[n];
    
    cout << "Enter " << n << " numbers: ";
    
    for(int i = 0; i < n; i ++){
        cin >> numbers[i];
    }
    
    // uses binary representation of i to loop through all possible
    // group combinations [0 , 2^n) ie 0 to 2^(n) - 1
    // ie if n = 3 i = 000, 001, 010, 011, 100, 101, 110, 111
    // where each bit represents which group the numbers will be split into
    // i if i = 3 or 011 
    // groupZero is: numbers[0]
    // groupOne is: numbers[1], numbers[2]
    for(int i = 0; i < pow(2, n); i++){// loop through all possible groups
        // init group totals
        groupZero = 0;
        groupOne = 0;
        mask = 1;// init bit mask (ie 0001)
        
        for(int j = 0; j < n; j++){// for each item in the numbers array
            int result = mask & i; // bitwise and (isolates bit for selection)
            // ie if mask is 0001 and i is 0101 result is 1
            // ie if mask is 0010 and i is 0101 result is 0
            if(result == 0){ // group based on isolated bit
                groupZero += numbers[j];
                setZero.insert(numbers[j]);
            }
            else{
                groupOne += numbers[j];
                setOne.insert(numbers[j]);
            }
            mask = mask << 1; // shifts mask bit left to isolate next position
            // ie (0001 becomes 0010)
        }
        
        if(groupZero == groupOne){ // check if groups are equal
            found = true; // mark solution as found
            break; // stop checking (exit loop)
        }
        // no solution found // clear sets for next group
        setZero.erase(setZero.begin(), setZero.end());
        setOne.erase(setOne.begin(), setOne.end());
        
    }
    
    cout << "Equal Group: ";
    if(found){ // Display results if solution found(bool)
        for(set<int>::iterator it = setZero.begin(); it!=setZero.end(); it++){
            cout << *it << " ";
        }
        cout << "vs ";
        for(set<int>::iterator it = setOne.begin(); it!=setOne.end(); it++){
            cout << *it << " ";
        }
    }
    else{ // display no solution found
        cout << "Not exist";
    }
    
    cout << endl << "Done." << endl;
    
    
    delete[] numbers;
    
    return 0;
}