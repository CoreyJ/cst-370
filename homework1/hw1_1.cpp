/*
* Title: hw1_1.cpp
* Abstract: Reads in a file name from the user then finds and displays the 
* minimum number found in the file and a list of distinct elements and a count
* of the number of times each distinct element is found in the file
* Author: Corey Johnson
* ID: 3333
* Date: 4/29/19
*/

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>

using namespace std;

int main(){
    
    string fileName = "";
    
    int count = 0;
    int minimunValue = 0; // holds index of min value
    
    int* inputData;
    int* inputCount;
    
    set<int> setA; //will sort and discard duplicates
    set<int>::iterator it; // set iterator

    cout << "Enter input file name: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
        
        inputFile >> count; // first line in data file holds input count
        
        inputData = new int[count]; //create aray to hold file data
        
        for(int i = 0; i < count; i++){
            inputFile >> inputData[i]; 
            if(inputData[i] < inputData[minimunValue]){
                minimunValue = i;
            }
            setA.insert(inputData[i]); // discards duplicates and sorts
        }
        
        inputFile.close(); // file close
        
        inputCount = new int[setA.size()]; // create array to hold counts of
                                           //unique elements
        
        for(int i = 0; i < setA.size(); i++){ // init count array
            inputCount[i] = 0;
        }
        
        for(int i = 0; i < count; i++){ // count each occurance
            inputCount[distance(setA.begin(), setA.find(inputData[i]))]++;
            // distance converts the iterator to an integer for
            // parallel array storage of the count
        }
        
        cout << "\nMin Number: " << inputData[minimunValue] << endl << endl;
        
        cout << "Number\tCount" << endl;
        
        for(it = setA.begin(); it != setA.end(); it++){
            cout << *it << "\t" 
                 << inputCount[distance(setA.begin(), it)] 
                 << endl;
        }
        
        delete[] inputData;
        delete[] inputCount;
    }
    else{ // file fails to open
        cout << "Unable to open file"; 
    }
        
    
    return 0;
    
}