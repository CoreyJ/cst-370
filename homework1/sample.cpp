/*
* Title: sample.cpp
* Abstract: This program displays Hello World and g++ version on the screen.
* Author: Corey Johnson
* ID: 3333
* Date: 4/27/19
*/

#include <iostream>
using namespace std;

int main(){
    
    cout << "Hello World!\n";
    cout << "g++ version: " << __GNUC__ << endl;
    
    return 0;
}