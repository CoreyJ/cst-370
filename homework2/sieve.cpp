/*
* Title: sieve.cpp
* Abstract: implementation of the sieve of Eratosthenes - a method for 
* generating consecutive primes
* Author: Corey Johnson
* ID: 3333
* Date: 5/6/19
*/
#include <iostream>
#include <math.h>

using namespace std;

int* sieve(int n){// returns array of prime numbers from 2 to n
    int* a = new int[n];
    
    for(int p = 2; p <= n; p++){ // init array from 2 to n
        a[p-2] = p;
    }
    
    for(int p = 2; p <= floor(sqrt(n)); p++){
        if(a[p-2] != 0){ // if p hasn't been eliminated
            int j = p * p; //calculate first multiple
            while(j <= n){
                a[j-2] = 0; // mark as eliminated
                j += p; // calculate next multiple
            }
        }
    }
    
    int* l = new int[n]; 
    
    for(int i = 0; i < n; i++){
        l[i] = 0;
    }
    
    int i = 0;
    
    for(int p = 2; p <= n; p++){// move non eliminated elements to new array
        if(a[p-2] != 0){
            l[i] = a[p-2];
            i++;
        }
    }
    
    delete[] a;
    
    return l;
}

int main(){
    
    int n = 0;
    
    cout << "Enter a number: ";
    cin >> n;
    
    int* primes = sieve(n);
    
    cout << "Prime numbers:";
    
    int i = 0;
    
    while(primes[i] != 0){
        if(i != 0){
            cout << ",";
        }
        cout <<" "<< primes[i++];
    }
    
    cout << endl;
    
    delete[] primes;
    
    return 0;
}

