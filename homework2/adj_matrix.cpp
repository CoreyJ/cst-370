/*
* Title: adj_matrix.cpp
* Abstract: Reads in a directed graph from an input file and outputs an 
* adjacency matrix
* Author: Corey Johnson
* ID: 3333
* Date: 5/6/19
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;


int main(){
    
    string fileName = "";
    
    cout << "Enter input file name: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
    
        int numVertices = 0;
        int numEdges = 0;
        
        inputFile >> numVertices >> numEdges;
        
        int* matrix = new int[numVertices * numVertices];
        
        for(int i = 0; i < numVertices * numVertices; i++){ // init matrix
            matrix[i] = 0;
        }
        
        int x, y;
        for(int i = 0; i < numEdges; i ++){ 
            inputFile >> x >> y;  // get edge from file
            matrix[numVertices * x + y] = 1; // mark edge in matrix
        }
        
        inputFile.close();
        
        cout << "Number of erticies: " << numVertices << endl;
        cout << "Number of edges: " << numEdges << endl;
        cout << "Adjacency matrix: ";
        
        
        for(int i = 0; i < numVertices * numVertices; i++){
            if(i % numVertices == 0){ // row start
                cout << endl << "   ";
            }
            cout << matrix[i] << " ";
        }
        
        cout << endl;
    
        delete[] matrix;
        
    }
    else{ // file fails to open
        cout << "Unable to open file"; 
    }
    
    return 0;
}