/*
* Title: ts.cpp
* Abstract: This program implements the topological sorting algorithm based
* on the source-removal algorithm. It accepts a file which contains a directed
* acyclic graph and outputs the topological sort.
* Author: Corey Johnson
* ID: 3333
* Date: 6/4/2019
*/

#include <iostream>
#include <fstream>
#include <string>


using namespace std;


int main(){
    
    string fileName = "";
    
    cout << "Enter filename: ";
    cin >> fileName;
    
    ifstream inputFile(fileName);
    
    if (inputFile.is_open()){ // check file open success
        
        int numVertex = 0;
        inputFile >> numVertex;
        
        int* DAG = new int [numVertex * numVertex];
        bool* removed = new bool[numVertex];
        
        for(int i = 0; i < numVertex; i++){
            removed[i] = false;
        }
        
        
        for(int i = 0; i < numVertex * numVertex; i++){
            inputFile >> DAG[i];
        }
        
        int remaining = numVertex;
        
        while(remaining > 0){ 
            for(int i = 0; i < numVertex; i++){
                if(removed[i] == false){ // if removed skip
                    bool source = true; // assume its a source vertex
                    for(int j = 0; j < numVertex; j++){
                        if(DAG[j * numVertex + i] == 1 && removed[j] == false){
                            source = false; //flag as not a source vertex
                            // if a remaining vertext points to it
                            break; // move to next vertex
                        }
                    }
                    if(source){// if still a source vertex
                        cout << i << " "; // output 
                        remaining--;
                        if(remaining > 0){
                            cout << "-> ";
                        }
                        removed[i] = true; // mark as removed
                        break; //restart looking for next source vertex
                    }
                }
            }
        }
        
        cout << endl;
        
        delete[] DAG;
        delete[] removed;
        
    }
    else{
        cout << "File not Found" << endl;
    }
    
    return 0;
}